# PSR Semestral project
###### Aleksandra Pereverzeva, Karafiát Vít
======
### Motor controlling

Application is meant to adjust the position of the slave motor depending on the position of the master motor using dynamical PWM control.

#### Getting Started
Clone git-repository using SSH git@gitlab.fel.cvut.cz:perevale/PSR.git and add the source files to your project in VxWorks IDE.
The following instructions will provide procedure for WindRiver.
```
1. Add files to your project.
2. Create two Build Targets, which should include:
    a) boss.c, engines.h, udp.c, udp.h -- for master.out;
    b) slave.c, engines.h, udp.c, udp.h, webpage.c, webpage.h -- for slave.out.
3. Compile these two targets.
4. Connect all the necessary devices according to the following scheme:
```

<img src="https://gitlab.fel.cvut.cz/perevale/PSR/raw/master/img/zapojeni.png" alt="zapojeni">
```
5. Upload each build output to the corresponding Developing Board.
6. Start each task on the corrensponding board
```
#### Built With
* VxWorks WindRinver


#### Code used
Used templates:
* https://support.dce.felk.cvut.cz/psr/cviceni/semestralka/#hints

#### Data-flow diagram
<img src="https://gitlab.fel.cvut.cz/perevale/PSR/raw/master/img/Data-flow.png" alt="diag">


#### Screenshot of the interface

<img src="https://gitlab.fel.cvut.cz/perevale/PSR/raw/master/img/sem.png" alt="scrn">
