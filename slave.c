#include "udp.h"
#include "engines.h"
#include "webpage.h"

irc_state_t irc;


volatile int received_state;

int listener();
void www(irc_state_t *irc,int *received_state);

/**
 * Debug function that prints status of slave motor and received state of boss motor.
 * \param num unused input
 */
void irc_print_status(int num)
{
			while (1) {
	                semTake(irc.irc_sem, WAIT_FOREVER);
	               printf("CURRENT:%d DESIRED:%d\n", irc.state, received_state);
	        }
}

/**
 * Funtction for FSM that increments or decrements the state of motor.
 */
void set_turns(){
	if(irc.lsa==-1||irc.lsb==-1){
		irc.lsa = irc.irc_a;
		irc.lsb = irc.irc_b;
	}else{
		irc.nsa = irc.irc_a;
		irc.nsb = irc.irc_b;
		if((irc.lsa==1)&&(irc.lsb==1)&&(irc.nsa==1)&&(irc.nsb==0)||
		  (irc.lsa==1)&&(irc.lsb==0)&&(irc.nsa==0)&&(irc.nsb==0)||
		  (irc.lsa==0)&&(irc.lsb==0)&&(irc.nsa==0)&&(irc.nsb==1)||
		  (irc.lsa==0)&&(irc.lsb==1)&&(irc.nsa==1)&&(irc.nsb==1)){
			irc.state--;
		}else if((irc.lsa==1)&&(irc.lsb==1)&&(irc.nsa==0)&&(irc.nsb==1)||
				(irc.lsa==0)&&(irc.lsb==1)&&(irc.nsa==0)&&(irc.nsb==0)||
				(irc.lsa==0)&&(irc.lsb==0)&&(irc.nsa==1)&&(irc.nsb==0)||
				(irc.lsa==1)&&(irc.lsb==0)&&(irc.nsa==1)&&(irc.nsb==1)){
			irc.state++;
		}
		irc.lsa = irc.nsa;
		irc.lsb = irc.nsb;
	}
}

/**
 * Function that generates PWM which on depends on the width received.
 * \param width PWM width to be used and also its orientation
 */
void pwm_width(int width)
{
	
	if(width>=0){//RIGHT

		(*(volatile uint32_t *) (0x43c20000+0x000C)) = width|(1<<30) ;
	}else{//LEFT

		width = -width;
		(*(volatile uint32_t *) (0x43c20000+0x000C)) = width|(2<<30);
	}
	}

/**
 * Inits PWM in registers.
 */
void init_pwm()
{
	(*(volatile uint32_t *) (0x43c20000+0x0000)) = 1<<6;
	(*(volatile uint32_t *) (0x43c20000+0x0008)) = 0x1388;
	
	
}
/**
 * Disables PWM in registers.
 */
void disable_pwm()
{
	(*(volatile uint32_t *) (0x43c20000+0x0000)) = 0<<6;
}

/**
 * Function reads registers for iterrupt. If interrupt is received than the semafor is given which is used for debug and
 * set_turns function is called. Status register is also reseted.
 * \param irc contains all information about motor.
 */
void irc_count(irc_state_t* irc){
	int sr;
	if (*(volatile uint32_t *) (0xE000A000 + 0x00000298) & (1 << irc->emio_pin)) {
	    *(volatile uint32_t *) (0xE000A000 + 0x00000298) = (1 << irc->emio_pin); /* reset (stat) */
		sr = *(volatile uint32_t *) (irc->irc_base_addr + 0x0004);
		
		irc->irc_a = (sr & 0x100) >> 8;
		irc->irc_b= (sr & 0x200) >> 9;
		semGive(irc->irc_sem);
		set_turns();
	}
}
/**
 * Calls irc_count.
 */
void irc_isr(void)
{
            irc_count(&irc);
}

/*
 *  Enable IRQ
 *
 *  See TRM, 14.2.4 Interrupt Function (pg. 391, pg. 1348). Technical reference
 *  manual link is on rtime HW wiki: https://rtime.felk.cvut.cz/hw/index.php/Zynq
 */
void irc_init(void)
{
// Interrupt Status (GPIO Bank2, EMIO)
        *(volatile uint32_t *) (0xE000A000 + 0x00000298) = (1 << 2); /* reset (stat) */
// Direction mode (GPIO Bank2, EMIO)     
        *(volatile uint32_t *) (0xE000A000 + 0x00000284) = 0x0; /* set as input (dirm) */
// Interrupt Type (GPIO Bank2, EMIO)        
        *(volatile uint32_t *) (0xE000A000 + 0x0000029c) = (1 << 2); /* rising edge (type) */
// Interrupt Polarity (GPIO Bank2, EMIO)     
        *(volatile uint32_t *) (0xE000A000 + 0x000002a0) = 0x0; /* rising edge (polarity) */
// Interrupt Any Edge Sensitive (GPIO Bank2, EMIO)     
        *(volatile uint32_t *) (0xE000A000 + 0x000002a4) = 0x0; /* rising edge (any) */
// Interrupt Enable/Unmask (GPIO Bank2, EMIO)       
        *(volatile uint32_t *) (0xE000A000 + 0x00000290) = (1 << 2); /* enable interrupt (en) GPIO2 GPIO6 */

        intConnect(INUM_TO_IVEC(52), irc_isr, 0);
        intEnable(52);
}
/*
 *  Disable IRQ
 */
void irc_disable(void)
{
        *(volatile uint32_t *) (0xE000A000 + 0x00000294) = 0x4; /* disable interrupt (dis) */

        intDisable(52);
        intDisconnect(INUM_TO_IVEC(52), irc_isr, 0);
}
/*
 *  Calculated PWM width from received boss state of motor and slave current motor state
 */
void turn(){
	while(1){ //(act!=req)
	pwm_width((received_state-irc.state)*12);
	struct timespec t1,t2;
	t1.tv_nsec=100000;
	t1.tv_sec=0;
	nanosleep(&t1,&t2);
	}
}

/*
 * Entry point for DKM.
 */
void motor_slave(void)
{
	printf("SLAVE\n");
		
	received_state=0;
	
		irc.irc_sem = semBCreate(SEM_Q_FIFO, 0);
	        
	    irc.state=0;
	        
	    irc.irc_base_addr = 0x43c20000;
	        
	    irc.emio_pin = 2;
	        
	    irc.lsa = -1;
	    irc.lsb = -1;
	        
	    TASK_ID st0;
	    TASK_ID st1;
	    TASK_ID st2;
	    

        irc_init();
        init_pwm();
        
        st0 = taskSpawn("irc_st", 100, 0, 4096, (FUNCPTR) irc_print_status, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        st1 = taskSpawn("Listener", 100, 0, 4096, (FUNCPTR) listener, &received_state, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        st2 = taskSpawn("Webpage", 100, 0, 4096, (FUNCPTR) www, &irc, &received_state, 0, 0, 0, 0, 0, 0, 0, 0);
        
        turn();
        
        printf("All is ready.\n");
        //taskDelay(1000000);
        printf("Out of play time.\n");

        //irc_disable();
        
        //taskDelete(st0);
        //taskDelete(st1);
}
