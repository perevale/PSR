#include "udp.h"

/**
 * Function listens for packets of port set in variable PORT containing status of boss motor. It then save the received state to varable received_state.
 * It is used in slave.
 * \param received_state current state of boss motor
 */
int listener(int *received_state)
{
  int sockd;
  struct sockaddr_in my_name, cli_name;
  char buf[MAX_BUF];
  int status;
  int addrlen;
  char ip_received[16];
  int fd;
  struct ifreq ifr;
  struct PSRMC_message_header head;
  struct PSRMC_status status_rec;

  fd = socket(AF_INET, SOCK_DGRAM, 0);

   /* I want to get an IPv4 IP address */
  ifr.ifr_addr.sa_family = AF_INET;

   /* I want IP address attached to "eth0" */
  strncpy(ifr.ifr_name, "gem0", IFNAMSIZ-1);

  ioctl(fd, SIOCGIFADDR, &ifr);

  close(fd);

   /* display result */
  char* my_ip_addres = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
  my_ip_addres[15] = '\0';
  printf("MY IP: %s\n",my_ip_addres);
  sockd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockd == -1)
  {
    perror("Socket creation error");
    exit(1);
  }

  /* Configure server address */
  my_name.sin_family = AF_INET;
  my_name.sin_addr.s_addr = INADDR_ANY;
  my_name.sin_port = htons(PORT);

  status = bind(sockd, (struct sockaddr*)&my_name, sizeof(my_name));

  addrlen = sizeof(cli_name);
  while(1){
	  status = recvfrom(sockd, buf, MAX_BUF, 0,
		  (struct sockaddr*)&cli_name, &addrlen);
	  inet_ntop(AF_INET, &cli_name.sin_addr, ip_received, sizeof(ip_received));
	
	  if (!(ip_received && ip_received[0])) {
		printf("IP RECEIVED IS NULL.\n");
	  }
	  ip_received[15] = '\0';

	  if(strcmp(my_ip_addres, ip_received)!=0){
		  memcpy(&head, buf, sizeof(struct PSRMC_message_header));
		  if (head.control_number == CONTROL_NUM && head.protocol_version == PROTOCOL_VER) {
			  memcpy(&status_rec, buf, sizeof(struct PSRMC_status));
			  if(*received_state!=status_rec.state){
			  *received_state=status_rec.state;
			  }
		  }
	  }
  }
  close(sockd);
  return 0;
}
/**
 * Function sends packets to port set in variable PORT containing status of boss motor.
 * It is used in boss.
 * \param received_state current state of boss motor
 */
int speaker(int state)
{
  int sockd;
  struct sockaddr_in my_addr, srv_addr;
  int count;
  struct PSRMC_status status;
  
  status.head.control_number = CONTROL_NUM;
  status.head.protocol_version = PROTOCOL_VER;
  status.state = state;

  /* Create a UDP socket */
  sockd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockd == -1)
  {
    perror("Socket creation error");
    exit(1);
  }

  /* Configure client address */
  my_addr.sin_family = AF_INET;
  my_addr.sin_addr.s_addr = INADDR_ANY;
  my_addr.sin_port = 0;

  bind(sockd, (struct sockaddr*)&my_addr, sizeof(my_addr));

  /* Set server address */
  srv_addr.sin_family = AF_INET;
  inet_aton("255.255.255.255", &srv_addr.sin_addr);
  srv_addr.sin_port = htons(PORT);

  sendto(sockd, &status, sizeof(struct PSRMC_status) , 0,
      (struct sockaddr*)&srv_addr, sizeof(srv_addr));

  close(sockd);
  return 0;
}

