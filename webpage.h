#ifndef WEBPAGE
#define WEBPAGE

#include "engines.h"

void www(irc_state_t *irc,int *received_state);

const char *HTTP =  "<html>\
<head>\
  <script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\
  <script type=\"text/javascript\">\
    google.charts.load('current', {'packages':['corechart']});\
    google.charts.setOnLoadCallback(drawChart);\
\
    function drawChart() {\
      var data = google.visualization.arrayToDataTable([\
        %s\
      ]);\
\
      var options = {\
        title: 'Motor controlling',\
        curveType: 'function',\
        legend: { position: 'bottom' }\
      };\
\
      var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));\
\
      chart.draw(data, options);\
    }\
  </script>\
</head>\
<body onload=\"setTimeout(function(){location.reload()}, 200);\">\
<body>\
  <div id=\"curve_chart\" style=\"width: 2000px; height: 1000px\"></div>\
</body>\
</html>";

    

#endif
