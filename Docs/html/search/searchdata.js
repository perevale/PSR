var indexSectionsWithContent =
{
  0: "abcdehilmnoprstuw",
  1: "ip",
  2: "bersuw",
  3: "dilmpstuw",
  4: "acehilnoprs",
  5: "cmps",
  6: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "defines",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Macros",
  6: "Pages"
};

