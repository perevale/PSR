var searchData=
[
  ['server_5fmax_5fconnections',['SERVER_MAX_CONNECTIONS',['../webpage_8c.html#a955f792be718a7788fe1c1ad20d2fcab',1,'webpage.c']]],
  ['server_5fport',['SERVER_PORT',['../webpage_8c.html#ac42367fe5c999ec6650de83e9d72fe8c',1,'webpage.c']]],
  ['set_5fturns',['set_turns',['../boss_8c.html#a9d5b7d36384dbd320b9c6760a6d5a915',1,'set_turns():&#160;boss.c'],['../slave_8c.html#a9d5b7d36384dbd320b9c6760a6d5a915',1,'set_turns():&#160;slave.c']]],
  ['slave_2ec',['slave.c',['../slave_8c.html',1,'']]],
  ['speaker',['speaker',['../boss_8c.html#a19083f84b87a2ddaa8e7a00b4e09a908',1,'speaker(int state):&#160;udp.c'],['../udp_8c.html#a19083f84b87a2ddaa8e7a00b4e09a908',1,'speaker(int state):&#160;udp.c'],['../udp_8h.html#a19083f84b87a2ddaa8e7a00b4e09a908',1,'speaker(int state):&#160;udp.c']]],
  ['state',['state',['../structirc__state__t.html#a89f234133d3efe315836311cbf21c64b',1,'irc_state_t::state()'],['../struct_p_s_r_m_c__status.html#a89f234133d3efe315836311cbf21c64b',1,'PSRMC_status::state()']]],
  ['status_5finformer',['status_informer',['../boss_8c.html#a5766250183d34a22a1fe9fa2f995ee2f',1,'boss.c']]]
];
