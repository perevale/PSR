//#define BOSS

#include <taskLib.h>
#include <stdio.h>
#include <kernelLib.h>
#include <semLib.h>
#include <intLib.h>
#include <iv.h>
#include <stdint.h>
#include <time.h>

#ifndef IRC_STATE
#define IRC_STATE

typedef struct {
	volatile int irc_a;
	volatile int irc_b;
	
	int lsa;
	int lsb;
	int nsa;
	int nsb;
	
	int state;
	
	SEM_ID irc_sem;
	
	unsigned irc_base_addr;
	
	unsigned emio_pin;
	
	unsigned int act_pos;
}irc_state_t;

#endif //IRC_STATE
