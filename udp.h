#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include "engines.h"

#define MAX_BUF 1024
#define PORT 666

#define CONTROL_NUM 0x414C4331UL
#define PROTOCOL_VER 0x00010000UL


struct PSRMC_message_header {
    uint32_t control_number;
    uint32_t protocol_version;
};

struct PSRMC_status {
    struct PSRMC_message_header head;
    int state;
};

int listener(int *received_state);
int speaker(int state);
