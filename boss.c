#include "udp.h"
#include "engines.h"

irc_state_t irc;

int speaker(int state);
/**
 * Send state of the motor over the UDP to slave.
 */
void status_informer(){
	while(1){
		struct timespec t1,t2;
		t1.tv_nsec=1000000;
		t1.tv_sec=0;
		nanosleep(&t1,&t2);
		speaker(irc.state);
	}
}
/**
 * Funtction for FSM that increments or decrements the state of motor.
 */
void set_turns(){
	if(irc.lsa==-1||irc.lsb==-1){
		irc.lsa = irc.irc_a;
		irc.lsb = irc.irc_b;
	}else{
		irc.nsa = irc.irc_a;
		irc.nsb = irc.irc_b;
		if((irc.lsa==1)&&(irc.lsb==1)&&(irc.nsa==1)&&(irc.nsb==0)||(irc.lsa==1)&&(irc.lsb==0)&&(irc.nsa==0)&&(irc.nsb==0)||(irc.lsa==0)&&(irc.lsb==0)&&(irc.nsa==0)&&(irc.nsb==1)||(irc.lsa==0)&&(irc.lsb==1)&&(irc.nsa==1)&&(irc.nsb==1)){
			irc.state--;
		}else if((irc.lsa==1)&&(irc.lsb==1)&&(irc.nsa==0)&&(irc.nsb==1)||(irc.lsa==0)&&(irc.lsb==1)&&(irc.nsa==0)&&(irc.nsb==0)||(irc.lsa==0)&&(irc.lsb==0)&&(irc.nsa==1)&&(irc.nsb==0)||(irc.lsa==1)&&(irc.lsb==0)&&(irc.nsa==1)&&(irc.nsb==1)){
			irc.state++;
		}
		irc.lsa = irc.irc_a;
		irc.lsb = irc.irc_b;
	}
}
/**
 * Function reads registers for iterrupt. If interrupt is received than the semafor is given which is used for debug and
 * set_turns function is called. Status register is also reseted.
 * \param irc contains all information about motor.
 */
void irc_count(irc_state_t* irc){
	int sr;
	if (*(volatile uint32_t *) (0xE000A000 + 0x00000298) & (1 << irc->emio_pin)) {
	    *(volatile uint32_t *) (0xE000A000 + 0x00000298) = (1 << irc->emio_pin); /* reset (stat) */
		sr = *(volatile uint32_t *) (irc->irc_base_addr + 0x0004);
		
		irc->irc_a = (sr & 0x100) >> 8;
		irc->irc_b= (sr & 0x200) >> 9;
		semGive(irc->irc_sem);
		set_turns();
	}
}
/**
 * Calls irc_count.
 */
void irc_isr(void)
{
            irc_count(&irc);
}

/*
 *  Enable IRQ
 *
 *  See TRM, 14.2.4 Interrupt Function (pg. 391, pg. 1348). Technical reference
 *  manual link is on rtime HW wiki: https://rtime.felk.cvut.cz/hw/index.php/Zynq
 */

void irc_init(void)
{
// Interrupt Status (GPIO Bank2, EMIO)
        *(volatile uint32_t *) (0xE000A000 + 0x00000298) = (1 << 2); /* reset (stat) */
// Direction mode (GPIO Bank2, EMIO)     
        *(volatile uint32_t *) (0xE000A000 + 0x00000284) = 0x0; /* set as input (dirm) */
// Interrupt Type (GPIO Bank2, EMIO)        
        *(volatile uint32_t *) (0xE000A000 + 0x0000029c) = (1 << 2); /* rising edge (type) */
// Interrupt Polarity (GPIO Bank2, EMIO)     
        *(volatile uint32_t *) (0xE000A000 + 0x000002a0) = 0x0; /* rising edge (polarity) */
// Interrupt Any Edge Sensitive (GPIO Bank2, EMIO)     
        *(volatile uint32_t *) (0xE000A000 + 0x000002a4) = 0x0; /* rising edge (any) */
// Interrupt Enable/Unmask (GPIO Bank2, EMIO)       
        *(volatile uint32_t *) (0xE000A000 + 0x00000290) = (1 << 2); /* enable interrupt (en) GPIO2 GPIO6 */

        intConnect(INUM_TO_IVEC(52), irc_isr, 0);
        intEnable(52);
}
/*
 *  Disable IRQ
 */
void irc_disable(void)
{
        *(volatile uint32_t *) (0xE000A000 + 0x00000294) = 0x4; /* disable interrupt (dis) */

        intDisable(52);
        intDisconnect(INUM_TO_IVEC(52), irc_isr, 0);
}

/*
 * Entry point for DKM.
 */
void motor_boss(void)
{
	printf("BOSS\n");
	
	irc.irc_sem = semCCreate(SEM_Q_FIFO, 0);
		        
	irc.state=0;
		        
	irc.irc_base_addr = 0x43c20000;
		        
	irc.emio_pin = 2;
		        
	irc.lsa = -1;
	irc.lsb = -1;

    irc_init();

    TASK_ID st0;


    st0 = taskSpawn("status_informer", 100, 0, 4096, (FUNCPTR) status_informer, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        
    printf("All is ready.\n");
    //taskDelay(1000000);
    //printf("Out of play time.\n");

    //irc_disable();
    //taskDelete(st0);
}
