#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <sockLib.h>
#include <string.h>

#include "webpage.h"

#define SERVER_PORT     80 /* Port 80 is reserved for HTTP protocol */
#define SERVER_MAX_CONNECTIONS  20

int old_entries[10][3];

/**
 * Moves forward in queue. First is going out and on last index is written whe new values.
 * \param current current state of slave motor
 * \param desired current state of boss motor
 */
void update_old_entries(int current,int desired){
	int var;
	for (var = 0; var < 9; ++var) {
		old_entries[var][0]=old_entries[var+1][0];
		old_entries[var][1]=old_entries[var+1][1];
		old_entries[var][2]=old_entries[var+1][2];

	}
	old_entries[9][0]=current;
	old_entries[9][1]=desired;
	old_entries[9][2]=(desired-current)*12;

}

/**
 * Create table that is used in the JavaScript for generating the graphs.
 * \param current current state of slave motor
 * \param desired current state of boss motor
 * \param time current time 
 */
char* table_maker(int current,int desired,int time){

	char *tab;
	size_t sz;
	
	char *new_table="['Time', 'Current', 'Desired', 'PWM Width'],"
			"['%.2f',%d,%d,%d],['%.2f',%d,%d,%d],"
			"['%.2f',%d,%d,%d],['%.2f',%d,%d,%d],"
			"['%.2f',%d,%d,%d],['%.2f',%d,%d,%d],"
			"['%.2f',%d,%d,%d],['%.2f',%d,%d,%d],"
			"['%.2f',%d,%d,%d],['%.2f',%d,%d,%d]";

	sz = snprintf(NULL, 0, new_table,
			(time-9)*0.2,old_entries[1][0],old_entries[1][1],old_entries[1][2],
			(time-8)*0.2,old_entries[2][0],old_entries[2][1],old_entries[2][2],
			(time-7)*0.2,old_entries[3][0],old_entries[3][1],old_entries[3][2],
			(time-6)*0.2,old_entries[4][0],old_entries[4][1],old_entries[4][2],
			(time-5)*0.2,old_entries[5][0],old_entries[5][1],old_entries[5][2],
			(time-4)*0.2,old_entries[6][0],old_entries[6][1],old_entries[6][2],
			(time-3)*0.2,old_entries[7][0],old_entries[7][1],old_entries[7][2],
			(time-2)*0.2,old_entries[8][0],old_entries[8][1],old_entries[8][2],
			(time-1)*0.2,old_entries[9][0],old_entries[9][1],old_entries[9][2],
			time*0.2,current,desired,(desired-current)*12);
	tab = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
	snprintf(tab, sz+1, new_table,
			(time-9)*0.2,old_entries[1][0],old_entries[1][1],old_entries[1][2],
			(time-8)*0.2,old_entries[2][0],old_entries[2][1],old_entries[2][2],
			(time-7)*0.2,old_entries[3][0],old_entries[3][1],old_entries[3][2],
			(time-6)*0.2,old_entries[4][0],old_entries[4][1],old_entries[4][2],
			(time-5)*0.2,old_entries[5][0],old_entries[5][1],old_entries[5][2],
			(time-4)*0.2,old_entries[6][0],old_entries[6][1],old_entries[6][2],
			(time-3)*0.2,old_entries[7][0],old_entries[7][1],old_entries[7][2],
			(time-2)*0.2,old_entries[8][0],old_entries[8][1],old_entries[8][2],
			(time-1)*0.2,old_entries[9][0],old_entries[9][1],old_entries[9][2],
			time*0.2,current,desired,(desired-current)*12);
	
	printf("%d %s\n",time,tab);


	update_old_entries(current,desired);
	
	return tab;
}


/**
 * Funtion for controling the webpage and responding to connections.
 * \param irc irc of slave motor containing information about state of slave motor
 * \param received_state current state of boss motor
 */
void www(irc_state_t *irc,int *received_state)
{
  int s;
  int newFd;
  struct sockaddr_in serverAddr;
  struct sockaddr_in clientAddr;
  int sockAddrSize;
  int time=0;
  
  int var;
  for (var = 0; var < 10; ++var) {
	  old_entries[var][0]=0;
	  old_entries[var][1]=0;
}

  sockAddrSize = sizeof(struct sockaddr_in);
  bzero((char *) &serverAddr, sizeof(struct sockaddr_in));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(SERVER_PORT);
  serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

  s=socket(AF_INET, SOCK_STREAM, 0);
  if (s<0)
  {
    printf("Error: www: socket(%d)\n", s);
    return;
  }


  if (bind(s, (struct sockaddr *) &serverAddr, sockAddrSize) == ERROR)
  {
    printf("Error: www: bind\n");
    return;
  }

  if (listen(s, SERVER_MAX_CONNECTIONS) == ERROR)
  {
    perror("www listen");
    close(s);
    return;
  }

  printf("www server running\n");

  while(1)
  {
    /* accept waits for somebody to connect and the returns a new file descriptor */
    if ((newFd = accept(s, (struct sockaddr *) &clientAddr, &sockAddrSize)) == ERROR)
    {
      perror("www accept");
      close(s);
      return;
    }
    
    printf("I AM HERE\n");
    char* table=table_maker(irc->state,*received_state,time);
    time++;
    
    
    
//    char* lol = "['Year', 'Sales', 'Expenses'],\
//        ['2004',  1000,      400],\
//        ['2005',  1170,      460],\
//        ['2006',  660,       1120],\
//        ['2007',  1030,      540]";
    char *buf;
    size_t sz;
    sz = snprintf(NULL, 0, HTTP,table);
    buf = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
    snprintf(buf, sz+1, HTTP,table);

    FILE *f = fdopen(newFd, "w");
    fprintf(f,buf);
    fclose(f);
    free(table);
    free(buf);

    /* The client connected from IP address inet_ntoa(clientAddr.sin_addr)
       and port ntohs(clientAddr.sin_port).

       Start a new task for each request. The task will parse the request
       and sends back the response.

       Don't forget to close newFd at the end */
  }
}
